import re
import os
import operator

__dirname = os.path.dirname(os.path.realpath(__file__))


def unique_words(line):
    return filter(lambda w: len(w), list(set(map(lambda w: w.lower(), re.sub(r'\n', ' ', re.sub(r'[\.\,]', '', line))
                                                 .split(' ')))))


def read_file(filename="text", min_val=1, min_len=2):
    with open(filename, 'r', encoding="utf-8") as file:
        txt = file.read()
        uniques = unique_words(txt)

    full_dict = {word: len(re.compile(word, flags=re.IGNORECASE).findall(txt)) for word in uniques}
    return {
        t[0]: t[1] for t in
        sorted(full_dict.items(), key=operator.itemgetter(1), reverse=True)
        if t[1] > min_val and len(t[0]) > min_len
    }


if __name__ == '__main__':
    print(read_file(os.path.join(__dirname, 'text'), 20, 3))

from decorators import selectors, private

@selectors({'get': ['data', 'size'], 'set': ['data', 'label']})
@private('data', 'size')
class E:
    def __init__(self, label, start):
        self.label = label
        self.data = start
        self.size = len(self.data)


@selectors({'get': ['data', 'size'], 'set': ['data', 'label']})
@private('data', 'size', 'label')
class D:
    def __init__(self, label, start):
        self.label = label
        self.data = start
        self.size = len(self.data)

    def double(self):
        for i in range(self.size):
            self.data[i] = self.data[i] * 2

    def display(self):
        print('{} => {}'.format(self.label, self.data))


if __name__ == '__main__':
    try:
        Y = E('Wrong label', ['a', 'b'])
    except Exception as e:
        print(e)

    X = D('X is', [1, 2, 3])
    X.display()
    X.double()
    X.display()
    try:
        print(X.data)
        print(X.size)
    except Exception as e:
        pass

    print(X.getData())

    X.setData([1,1,1])
    assert X.getData() == [1,1,1]
    X.double()
    X.display()
    assert X.getData() == [2,2,2]

import types


def getterFactory(field):
    def getter(self):
        return self.__dict__[field]
    return 'get{}'.format(field.capitalize()), getter


def setterFactory(field):
    def setter(self, value):
        self.__dict__[field] = value
    return 'set{}'.format(field.capitalize()), setter


def selectors(settings):
    GETTERS = settings['get']
    SETTERS = settings['set']

    def wrapper(cls):
        def onInstance(*args, **kwargs):
            wrapped = cls(*args, **kwargs)

            PRIVATES = wrapped.privates
            if len(set(GETTERS) - set(PRIVATES)) > 0:
                raise TypeError("cannot create a getter for a non public field")

            if len(set(SETTERS) - set(PRIVATES)) > 0:
                raise TypeError("cannot create a setter for a non public field")

            for g in GETTERS:
                name, meth = getterFactory(g)
                wrapped.wrapped.__dict__[name] = types.MethodType(meth, wrapped.wrapped)

            for s in SETTERS:
                name, meth = setterFactory(s)
                wrapped.wrapped.__dict__[name] = types.MethodType(meth, wrapped.wrapped)

            return wrapped
        return onInstance
    return wrapper


def private(*privates):
    def wrapper(cls):
        class onInstance:
            def __init__(self, *args, **kwargs):
                self.wrapped = cls(*args, **kwargs)
                self.privates = privates

            def __getattr__(self, attr):
                if attr in privates:
                    raise TypeError("cannot access private field " + attr)
                else:
                    return getattr(self.wrapped, attr)

            def __setattr__(self, key, value):
                if key in 'wrapped':
                    self.__dict__[key] = value
                elif key in privates:
                    raise TypeError("cannot directly change " + key)
                else:
                    setattr(self.wrapped, key, value)

        return onInstance
    return wrapper

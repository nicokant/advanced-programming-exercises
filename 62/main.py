import pickle
import os
import functools
import inspect


def call_to_hex(fn_name, args):
    return pickle.dumps((fn_name, args)).hex()


def in_cache(cls, key):
    setattr(cls, '__cache', {}) if not getattr(cls, '__cache', False) else None
    return key in cls.__cache.keys()


def memoization(method):

    @functools.wraps(method)
    def wrapper(inst, *args):

        h = call_to_hex(method.__name__, *args)
        if in_cache(inst.__class__, h):
            return inst.__class__.__cache[h]
        else:
            res = method(inst, *args)
            inst.__class__.__cache[h] = res
            return res
    return wrapper


__dirname = os.path.dirname(os.path.realpath(__file__))


def logging(filename):

    def log_wrapper(method):
        @functools.wraps(method)
        def wrapped(inst, *args, **kwargs):
            file = open(os.path.join(__dirname, filename), 'a')
            file.write('method: {} - args: {}\n'.format(method.__name__, *args))
            file.close()
            return method(inst, *args, **kwargs)

        return wrapped
    return log_wrapper


def trace(method):
    @functools.wraps(method)
    def wrapper(inst, *args, **kwargs):
        print(inspect.getouterframes(inspect.currentframe()))
        return method(inst, *args, **kwargs)

    return wrapper


call_fib = 0
call_fact = 0


class MyMath:

    @logging('fib.log')
    @memoization
    @logging('fib-post.log')
    def fib(self, n):
        global call_fib
        call_fib += 1
        return 0 if n == 0 else 1 if n == 1 else self.fib(n - 1) + self.fib(n - 2)

    @memoization
    @trace
    @logging('fact.log')
    def fact(self, n):
        global call_fact
        call_fact += 1
        return 1 if n == 0 else n * self.fact(n - 1)


if __name__ == "__main__":
    math = MyMath()

    math.fib(20)
    assert call_fib == 21


    math.fact(20)
    assert call_fact == 21
    math.fact(21)
    assert call_fact == 22



import os, re

MAX_LEN = 74
START_POS = 35

class KWIC:
    def __init__(self, filename):
        self.file = open(filename, 'r')
        self.lines = [line.strip() for line in self.file.readlines()]

        self.words = sorted([(l_num, word) for l_num, line in enumerate(self.lines) for word in line.lower().split(' ') if len(word) > 2 and word != "the"], key=lambda t: t[1])

    def __str__(self):
        for line_index, word in self.words:
            word_in_line_index = re.search(word, self.lines[line_index], re.IGNORECASE).start()

            print('\t{}{}{}'.format(line_index + 1, ' ' * (START_POS - word_in_line_index), self.lines[line_index]))

        return ''

if __name__ == '__main__':
    kwic = KWIC( os.path.join(os.path.dirname(__file__), 'texts'))
    print(kwic)
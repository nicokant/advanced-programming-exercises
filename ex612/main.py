import operator
import datetime


class Person:
    def __init__(self, fname, lname, bday):
        self.name = fname
        self.lastname = lname
        self.birthday = bday

    def __str__(self):
        return "name: {}\nlastname: {}\nbday: {}".format(self.name, self.lastname, self.birthday)


class GradeDescriptor():
    def __get__(descInst, clsInst, cls):
        return sum(clsInst.lectures.values()) / len(clsInst.lectures)


class Student(Person):
    def __init__(self, *args):
        super(Student, self).__init__(*args)
        self.lectures = {}

    grade_average = GradeDescriptor()


class SalaryDescriptor:
    def __init__(self, field, mul):
        self.field = field
        self.mul = mul

    def __get__(self, instance, owner):
        return instance.__dict__[self.field] * self.mul

    def __set__(self, instance, value):
        instance.__dict__[self.field] = value / self.mul


class Worker(Person):
    day_salary = SalaryDescriptor('pay_per_hour', 8)
    week_salary = SalaryDescriptor('day_salary', 5)
    month_salary = SalaryDescriptor('week_salary', 4)
    year_salary = SalaryDescriptor('month_salary', 12)

    def __init__(self, *args):
        super(Worker, self).__init__(*args)
        self.pay_per_hour = 10



class AgeDescriptor:
    def __get__(self, instance, owner):
        return (datetime.date.today() - instance.birthday).days

    def __set__(self, instance, value):
        instance.birthday = datetime.date.today() - datetime.timedelta(days=value)


class Wizard(Person):
    def __init__(self, *args):
        super(Wizard, self).__init__(*args)

    age = AgeDescriptor()


if __name__ == '__main__':
    john = Student('john', 'john', datetime.date.today())
    john.lectures["math"] = 10
    assert john.grade_average == 10
    john.lectures["cs"] = 5
    assert john.grade_average == 7.5

    jim = Worker('jim', 'jim', datetime.date.today())
    assert jim.day_salary == 10 * 8
    jim.pay_per_hour = 20
    assert jim.day_salary == 20 * 8
    jim.day_salary = 240
    assert jim.pay_per_hour == 30

    merlin = Wizard('merlin', 'wiz', datetime.date(2019, 2, 11))
    assert merlin.age == (datetime.date.today() - datetime.date(2019, 2, 11)).days
    merlin.age = 16
    assert merlin.birthday == datetime.date.today() - datetime.timedelta(days=16)

import operator
import datetime

class Person:
    def __init__(self, fname, lname, bday):
        self.name = fname
        self.lastname = lname
        self.birthday = bday

    def __str__(self):
        return "name: {}\nlastname: {}\nbday: {}".format(self.name, self.lastname, self.birthday)


class Student(Person):
    def __init__(self, *args):
        self.lectures = {}

    @property
    def grade_average(self):
        return sum(self.lectures.values()) / len(self.lectures)


class Worker(Person):
    def __init__(self, *args):
        self.pay_per_hour = 10

    @property
    def day_salary(self):
        return self.pay_per_hour * 8

    @day_salary.setter
    def day_salary(self, v):
        self.pay_per_hour = v / 8

    @property
    def week_salary(self):
        return self.day_salary * 5

    @week_salary.setter
    def week_salary(self, v):
        self.day_salary = v / 5

    @property
    def month_salary(self):
        return self.week_salary * 4

    @month_salary.setter
    def month_salary(self, v):
        self.week_salary = v / 4

    @property
    def year_salary(self):
        return self.month_salary * 12

    @year_salary.setter
    def year_salary(self, v):
        self.month_salary = v / 12

    def __str__(self):
        return "{}\nhour: {}\nday: {}\nweek: {}\nmonth: {}\nyear: {}".format(super(Worker, self).__str__(),
                                                                             self.pay_per_hour,
                                                                             self.day_salary, self.week_salary,
                                                                             self.month_salary, self.year_salary)


class Wizard(Person):
    def __init__(self, *args):
        super(Wizard, self).__init__(*args)

    @property
    def age(self):
        return (datetime.date.today() - self.birthday).days

    @age.setter
    def age(self, v):
        self.birthday = datetime.date.today() - datetime.timedelta(days=v)


if __name__ == '__main__':
    john = Student('john', 'john', datetime.date.today())
    john.lectures["math"] = 10
    assert john.grade_average == 10
    john.lectures["cs"] = 5
    assert john.grade_average == 7.5

    jim = Worker('jim', 'jim', datetime.date(2019, 2, 11))
    assert jim.day_salary == 10 * 8
    jim.pay_per_hour = 20
    assert jim.day_salary == 20 * 8
    jim.day_salary = 240
    assert jim.pay_per_hour == 30

    merlin = Wizard('merlin', 'wiz', datetime.date(2019, 2, 11))
    assert merlin.age == (datetime.date.today() - datetime.date(2019, 2, 11)).days
    merlin.age = 16
    assert merlin.birthday == datetime.date.today() - datetime.timedelta(days=16)

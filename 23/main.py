import math
from functools import reduce
from operator import mul


def fac(n):
    return reduce(mul, range(1, n + 1))


def taylor_sin(x, approx):
    return sum([((-1**i) * (x**((2 * i) + 1))) / fac((2 * i) + 1) for i in range(approx)])


## Not clear how to test it... ##

print([(math.asin(x) + taylor_sin(x, 50)) for x in [n * 0.1 for n in range(0, 11)]])

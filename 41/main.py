import re
import itertools


class MyString(str):
    def __init__(self, value):
        self.value = value

    def is_palindrome(self):
        newString = re.sub(r'\W', '', self.value).lower()
        return newString == newString[::-1]

    def substruct(self, anotherString):
        return filter(lambda l: not l in anotherString, self.value)

    def check_anagrams(self, words):
        return dict((w,w in [''.join(w) for w in itertools.permutations(self.value, len(self.value))]) for w in words)


if __name__ == '__main__':
    print(MyString("Do geese see God?").is_palindrome())
    w = MyString("Walter Cazzola")
    subs = 'abcwxyz'

    print(list(w.substruct(subs)))

    print(MyString('abcd').check_anagrams(['abdc', 'cabd', 'osso']))

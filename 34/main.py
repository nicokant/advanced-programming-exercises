from operator import itemgetter


def sort_dict(d): return sorted(d.items(), key=itemgetter(0))


class KeyOrderedDict(dict):
    def __init__(self, diction):
        for el in sort_dict(diction):
            dict.__setitem__(self, el[0], el[1])

    def __setitem__(self, key, value):
        dict.__setitem__(self, key, value)
        old_values = sort_dict(self)

        keys = [n for n in self.keys()]
        for k in keys:
            dict.__delitem__(self, k)

        self.__init__(dict(old_values))


if __name__ == '__main__':
    test = KeyOrderedDict({ 'ciao': 'a', 'a': 'a', 'z': 'z' })
    test["prova"] = "prova"
    print(test)
    test["b"] = "b"
    print(test)
# Exercise 4: Playing around with Extensions.
Python's dictionaries do not preserve the order of inserted data nor store the data sorted by the key. Write an extension for the dict class whose instances will keep the data sorted by their key value. 

**Note** that the order must be preserved also when new elements are added.
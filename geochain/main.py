from os import path as opath
from itertools import permutations
from functools import reduce
from operator import itemgetter

_dirname = opath.dirname(opath.realpath(__file__))

countries = (s.lower().strip() for s in open(opath.join(_dirname, 'countries'), 'r').readlines())

edges = set(filter(lambda e: e[0][-1] == e[1][0], permutations(countries, 2)))


def major(a1, a2):
    return a1 if len(a1) >= len(a2) else a2


def path(start_node, left_edges=edges):
    startwith_edges = set(filter(lambda e: e[0] == start_node, left_edges))
    no_start = set(filter(lambda e: start_node not in e, left_edges))

    if len(startwith_edges) == 0:
        return [start_node]
    else:
        return [start_node] + reduce(major, [path(e1, no_start) for e1 in map(itemgetter(1), startwith_edges)])


if __name__ == '__main__':
    print(path('italy'))
    print(path('belgium'))
    print(path('spain'))

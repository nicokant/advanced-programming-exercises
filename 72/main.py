import itertools


def prime_numbers_less_than(n):
    candidates = [(num, [number for number in range(2, num) if num % number == 0]) for num in range(2, n)]
    return list(map(lambda o: o[0], filter(lambda o: len(o[1]) == 0, candidates)))


def goldbach(num):
    primes = prime_numbers_less_than(num)
    combs = itertools.combinations(primes, 2)
    return list(filter(lambda n: n[0] + n[1] == num, combs))


def goldbach_list(n, m):
    return {i: goldbach(i) for i in range(n,m) if i % 2 == 0}


if __name__ == '__main__':
    print(goldbach(150))
    print(goldbach_list(40, 100))


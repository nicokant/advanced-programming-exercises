
OPS = ('+', '-', '*', '/', '**', '|', '&', '!')


class PolishCalculator:
    def __init__(self, string):
        self.string = string.split(" ")

    def __str__(self):
        stack = []

        for e in self.string:
            if e not in OPS:
                stack.append(e)
            else:
                stack.append("({}{}{})".format(stack.pop(-2), e, stack.pop()))

        return "".join(stack)[1:-1]

    def eval(self):
        stack = []

        for e in self.string:
            if e not in OPS:
                stack.append(e)
            else:
                stack.append(eval("({}{}{})".format(stack.pop(-2), e, stack.pop())))

        return stack[0]


if __name__ == '__main__':
    pc = PolishCalculator("3 4 + 5 *")
    assert pc.eval() == 35
    assert str(pc) == "(3+4)*5"

    pc1 = PolishCalculator("1 7 3 / 1 4 - 2 * / +")
    print(str(pc1))

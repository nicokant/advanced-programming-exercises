# Advanced Programming

My solutions to the exercises of the Advanced Programming course at Università degli studi di Milano.
Each directory contains the solution (or an attempt) for each exercise.
- [X] 2.1
- [X] 2.2
- [X] 2.3 (Partial)
- [X] 3.1
- [ ] 3.2 (Attempted)
- [X] 3.3
- [X] 3.4
- [X] 4.1
- [X] 4.2
- [X] 4.3
- [X] 6.1
- [X] 6.2
- [X] 6.3
- [X] 7.1
- [X] 7.2
- [ ] 7.3


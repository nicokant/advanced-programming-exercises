
class Vertex:
    def __init__(self, name):
        self.name = name

    def __str__(self):
        return self.name

    def __repr__(self):
        return self.name


class Edge:
    def __init__(self, v1, v2):
        if v1 != v2:
            self.edge = (v1, v2)
        else:
            raise Exception("can't create a self edge")

    def __contains__(self, item):
        return item in self.edge

    def related(self, v):
        return self.edge[1] if v == self.edge[0] else self.edge[0]

    def __str__(self):
        return "{} - {}".format(self.edge[0], self.edge[1])

    def __repr__(self):
        return "{} - {}".format(self.edge[0], self.edge[1])


class Graph:
    def __init__(self):
        self.vertices = set()
        self.edges = list()

    def add_vertex(self, v):
        self.vertices.add(v)

    def add_edge(self, e):
        v1, v2 = e.edge
        if v1 in self.vertices and v2 in self.vertices:
            self.edges.append(e)

    def remove_vertex(self, v):
        self.vertices.remove(v)
        self.edges = list(filter(lambda edge: v not in edge, self.edges))

    def __str__(self):
        visited = set()
        start_vert = self.vertices.pop()

        def visit(vert):
            res = "{}".format(vert)

            edges_to_visit = set()
            visited.add(vert)

            connected_edges = list(filter(lambda e: vert in e and e not in visited, self.edges))
            edges_to_visit.update(set(connected_edges))
            for e in edges_to_visit:
                visited.add(e)

                connected = e.related(vert)
                if connected not in visited:
                    nested = visit(connected)
                    res += "\n{}".format(nested)

            return res

        r = visit(start_vert)
        return r


if __name__ == '__main__':
    facebook = Graph()

    mario = Vertex("mario")
    luigi = Vertex("luigi")
    marco = Vertex("marco")
    luca = Vertex("luca")
    anna = Vertex("anna")
    john = Vertex("john")

    r1 = Edge(marco, luigi)
    r2 = Edge(luigi, mario)
    r3 = Edge(luca, mario)
    r4 = Edge(luca, anna)
    r5 = Edge(john, anna)
    r6 = Edge(mario, john)
    r7 = Edge(john, luca)

    facebook.add_vertex(mario)
    facebook.add_vertex(marco)
    facebook.add_vertex(luigi)
    facebook.add_vertex(luca)
    facebook.add_vertex(anna)
    facebook.add_vertex(john)

    facebook.add_edge(r1)
    facebook.add_edge(r2)
    facebook.add_edge(r3)
    facebook.add_edge(r4)
    facebook.add_edge(r5)
    facebook.add_edge(r6)
    facebook.add_edge(r7)

    print(facebook)

    print("\n\tremoving john from the graph\n")
    facebook.remove_vertex(john)

    print(facebook)

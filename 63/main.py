import datetime

# Add ex 6.1.2 to the python path before importing
import sys
from os import path
__dir = path.dirname(path.realpath(__file__))
sys.path.append(path.dirname(__dir))

from ex612.main import Person as Pers, Worker as Wor
import types
import functools

class Counter(type):
    def __new__(mcs, classname, supers, classdict):

        default__init__ = classdict["__init__"]

        def ext__init__(self, *args, **kwargs):
            setattr(self.__class__, 'counter', 0) if not getattr(self.__class__, 'counter', False) else None
            self.__class__.counter += 1
            return default__init__(self, *args, **kwargs)

        classdict["__init__"] = ext__init__

        return type.__new__(mcs, classname, supers, classdict)


class Spell(type):
    def __new__(mcs, classname, supers, classdict, **kwargs):
        if classname == 'Person':
            return type('Worker', (Wor, ), classdict)

        return type.__new__(mcs, classname, supers, classdict)


def multiTriggered(count=2):
    def wrapped(fn):
        @functools.wraps(fn)
        def wrapper(inst, *args, **kwargs):
            attr = '{}_active'.format(fn.__name__)
            if hasattr(inst, attr):
                setattr(inst, attr, getattr(inst, attr) + 1)
                if getattr(inst, attr) >= count:
                    return fn(inst, *args, **kwargs)
            else:
                setattr(inst, attr, 1)

        return wrapper
    return wrapped


class MultiTriggeredMethod(type):
    def __new__(mcs, classname, supers, classdict):
        fns = [fn for fn in classdict.items() if type(fn[1]) is types.FunctionType and fn[0] != "__init__"]

        for name, fn in fns:
            classdict[name] = multiTriggered(3)(fn)

        return type.__new__(mcs, classname, supers, classdict)


if __name__ == '__main__':

    class Person(Pers, metaclass=Counter):
        def __init__(self, *args, **kwargs):
            super().__init__(*args, **kwargs)


    Person('jim', 'jim', datetime.date(2018, 11, 22))
    Person('jim', 'jim', datetime.date(2018, 11, 22))

    assert Person.counter == 2

    class Person(Pers, metaclass=Spell):
        def __init__(self, *args, **kwargs):
            super().__init__(*args, **kwargs)


    p = Person('jim', 'jim', datetime.date(2018, 11, 22))
    assert isinstance(p, Wor)
    assert p.pay_per_hour == 10
    assert p.day_salary == 80

    import inspect
    assert not inspect.isgetsetdescriptor(p.day_salary)
    assert not inspect.ismethoddescriptor(p.day_salary)
    assert not inspect.isgetsetdescriptor(p.day_salary)
    assert isinstance(p.day_salary, int)

    class Person(Pers, metaclass=MultiTriggeredMethod):
        def __init__(self, *args, **kwargs):
            super().__init__(*args, **kwargs)

        def a_method(self):
            return 'called'

    p = Person('jim', 'jim', datetime.date(2018, 11, 22))

    assert p.a_method() is None
    assert p.a_method() is None
    assert p.a_method() == 'called'
    assert p.a_method() == 'called'

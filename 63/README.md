# Exercise 3: Metaclasses.
Let us consider the class Person again and implement the following metaclasses:

- The metaclass Counter which counts how many times Person has been instantiated
- The metaclass Spell that transforms the instances of Person in a Worker with the properties/descriptors of the previous exercise as real methods/attributes
- The metaclass MultiTriggeredMethod that let activate a method only when called twice
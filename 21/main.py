import sys
from functools import reduce

sys.setrecursionlimit(10000)


def smallest_divisible(min_num, max_num):
    return reduce(lambda acc, n: acc % n != 0 and acc * n or acc, range(min_num, max_num+1))


def sum_figures(n, p):
    return sum([int(nm) for nm in str(pow(n, p))])


def first_fibonacci(max_num, a=0, b=1):
    return len(str(a)) == max_num and a or first_fibonacci(max_num, b, a + b)


if __name__ == '__main__':
    print(sum(filter(lambda n: (n % 3) == 0 or (n % 5) == 0, range(1000))))
    print(smallest_divisible(1, 20))
    print(sum_figures(2, 1000))
    print(first_fibonacci(1000))

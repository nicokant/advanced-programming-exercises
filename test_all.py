#!/bin/python3

import os
import subprocess

ERRORS = set()

__dirname = os.path.dirname(os.path.realpath(__file__))
dirs = [dr for dr in os.listdir(__dirname) if not dr.startswith('.') and os.path.isdir(os.path.join(__dirname, dr))]


for dr in dirs:
    try:
        print('checking {}'.format(dr))
        exe = os.path.join(__dirname, dr, 'main.py')
        subprocess.run(['python3', exe], check=True, stdout=subprocess.DEVNULL, stderr=subprocess.STDOUT)
    except subprocess.CalledProcessError as e:
        ERRORS.add(dr)


if len(ERRORS) > 0:
    print("Errors occurred in {}".format(','.join(list(ERRORS))))
    exit(1)
else:
    print("All tests passed")
    exit(0)

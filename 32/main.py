### INCOMPLETE ###

import itertools


class Monoid:
    def __init__(self, target_set, operation, identity):
        self.math_set = target_set
        self.op = operation
        self.ident = identity

    def check_identity(self):
        return all(self.op(self.ident, e) == self.op(e, self.ident) for e in self.math_set)

    def check_closure(self):
        return all(self.op(e1, e2) in self.math_set for e1, e2 in itertools.permutations(self.math_set, 2))


class Group:
    pass


class Ring:
    pass


if __name__ == '__main__':
    bool_monoid = Monoid(set([True, False]), lambda a,b: a or b, False)
    assert bool_monoid.check_identity()
    assert bool_monoid.check_closure()
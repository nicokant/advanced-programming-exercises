def val_or_zero(arr, index):
    ''' check if index access is negative - python allows it '''
    if index < 0 or index >= len(arr):
        return 0
    else:
        return arr[index]


def back(iterable):
    ''' Inverse operation of next '''
    return iterable.__back__()


class PascalTriangle:
    def __init__(self, *args, **kwargs):
        self.stage = 0
        self.iter_pos = 0
        self.algebra = kwargs.get("algebra") if kwargs.get("algebra") else lambda n1, n2: n1 + n2

        initial = kwargs.get('initial', 1)

        self.triangle = [[initial]]

    def __iter__(self):
        return self

    def _sum_prev(self, i):
        return self.algebra(val_or_zero(self.triangle[self.stage], i-1), val_or_zero(self.triangle[self.stage], i))

    def _gen_next(self):
        next_row = [self._sum_prev(i) for i in range(0, len(self.triangle[self.stage]) + 1)]
        self.stage += 1
        self.triangle.append(next_row)

    def __next__(self):
        self.iter_pos += 1

        if self.stage >= self.iter_pos:
            return iter(self.triangle[:self.iter_pos + 1])
        else:
            self._gen_next()
            return iter(self.triangle)

    def __back__(self):
        if self.iter_pos > 0:
            self.iter_pos -= 1

        return iter(self.triangle[:self.iter_pos + 1])


def alphabet(n1, n2):
    mod = ord('A') - 1

    tn1 = ord(n1) % mod if n1 is not 0 else 0
    tn2 = ord(n2) % mod if n2 is not 0 else 0

    return chr(tn1 + tn2 + mod)


if __name__ == '__main__':
    p = PascalTriangle(algebra=alphabet, initial='A')

    print(list(next(p)))
    print(list(next(p)))
    print(list(next(p)))
    print(list(next(p)))
    print(list(next(p)))
    print(list(next(p)))
    print(list(next(p)))





import math


class Triangle:
    def __init__(self, l):
        self._l = l

    def calculate_area(self):
        return (self._l ** 2) * math.sqrt(3) / 4

    def calculate_perimeter(self):
        return self._l * 3


class Square:
    def __init__(self, l):
        self._l = l

    def calculate_area(self):
        return self._l ** 2

    def calculate_perimeter(self):
        return self._l * 4


class Circle:
    def __init__(self, r):
        self._r = r

    def calculate_area(self):
        return (self._r ** 2) * math.pi

    def calculate_perimeter(self):
        return self._r * math.pi * 2


class Rectangle:
    def __init__(self, b, h):
        self._b = b
        self._h = h

    def calculate_area(self):
        return self._b * self._h

    def calculate_perimeter(self):
        return self._b * 2 + self._h * 2


class Pentagon:
    def __init__(self, l, a):
        self._l = l
        self._a = a

    def calculate_area(self):
        return self.calculate_perimeter() * self._a / 2

    def calculate_perimeter(self):
        return self._l * 5


if __name__ == '__main__':
    t = Triangle(2)
    s = Square(2)
    r = Rectangle(2, 3)
    c = Circle(1.3)
    p = Pentagon(4, 2)

    figures = [t, c, r, s, p]

    sp = sorted(figures, key=lambda f: f.calculate_perimeter())
    sa = sorted(figures, key=lambda f: f.calculate_area())

    print("sorted by perimeter")
    print(sp)

    print("sorted by area")
    print(sa)


    class Hexagon:
        def __init__(self, l, a):
            self._l = l
            self._a = a

        def calculate_area(self):
            return self.calculate_perimeter() * self._a / 2

        def calculate_perimeter(self):
            return self._l * 6

    print("adding an exagon")
    ex = Hexagon(1, 2)
    figures.append(ex)

    sp = sorted(figures, key=lambda f: f.calculate_perimeter())
    sa = sorted(figures, key=lambda f: f.calculate_area())

    print("sorted by perimeter")
    print(sp)

    print("sorted by area")
    print(sa)

    def geometry_iterator(geo_list):
        return iter(sorted(geo_list, key=lambda f: f.calculate_area()))

    geos = geometry_iterator(figures)
    print(next(geos).calculate_area())

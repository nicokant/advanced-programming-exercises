# Exercise 1: Playing around with Geometry.
To implement the classes representing: equilateral triangles, circles, rectangles, squares and pentagons with the following characteristics/properties/capabilities.

- they should understand the calculate_area() and calculate_perimeter() messages with the obvious meaning
- the state must be private

- a list of geometric shapes must be sortable by area and by perimeter (not at the same time, of course)
- to add an hexagon class should maintain all the capabilities of the existing classes and correctly interact with them
- to write a iterator that permits to return the elements of a list of geometric shapes sorted by increasing areas.